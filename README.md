# Controle de Orçamento v1.0.0

Sistema utilizado para controle e gerenciamento de orçamento individual
desenvolvido como projeto exemplo para o módulo de DevOps do curso de
especialização Lato Sensu "Desenvolvimento de Software para a Web".

## Arquitetura do Sistema

O sistema é composto de um módulo *frontend*, que utiliza o *framework* Angular
13, e de um módulo *backend* que utiliza o *framework* Spring Boot 2.

Para a persistência dos dados, o sistema utiliza o PostgreSQL.

## Executando o sistema

A execução do sistema necessita que os dois módulos (*backend* e *frontend*) estejam inicializados. Primeiro inicialize o *backend*.

### Inicializando o Backend

A inicialização do Backend pode ser realizada entrando no diretório `backend`:

```
cd backend
```

E posteriormente usando o *wrapper* do Gradle:

```
./gradlew bootRun
```

O módulo será inicializado na porta 8081 da máquina, podendo sua API ser acessada em `http://localhost:8081`

### Inicializando o Frontend

A inicialização do Frontened pode ser realizada entrando no diretório `frontend`:

```
cd frontend
```

Devem ser instaladas as dependências do sistema com o comando a seguir:

```
npm install
```

Na sequência, é o sistema deve ser inicializado com seguinte comando:

```
ng serve
```

O módulo será inicializado na porta 80 da máquina, e poderá ser acesso em `http://localhost:80`

## Licença
[MIT](https://choosealicense.com/licenses/mit/)