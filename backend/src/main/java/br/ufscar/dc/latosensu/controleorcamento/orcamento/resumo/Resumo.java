package br.ufscar.dc.latosensu.controleorcamento.orcamento.resumo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import br.ufscar.dc.latosensu.controleorcamento.orcamento.categoria.Categoria;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.Lancamento;
import lombok.Data;

@Data
public class Resumo {

    List<ItemResumo> itensResumo;

    public Resumo(Map<Categoria, List<Lancamento>> lancamentosPorCategoria) {
        this.itensResumo = computarItensResumo(lancamentosPorCategoria);
    }

    private List<ItemResumo> computarItensResumo(Map<Categoria, List<Lancamento>> lancamentosPorCategoria) {
        List<ItemResumo> itensResumo = new ArrayList<ItemResumo>();
        lancamentosPorCategoria.entrySet().stream().forEach((item) -> {
            Optional<BigDecimal> valorCategoria = item.getValue().stream().map(Lancamento::getValor).reduce((BigDecimal acumulado, BigDecimal valor)->{
                return acumulado.add(valor);
            });
            Categoria categoria = item.getKey();
            ItemResumo itemResumo = new ItemResumo(categoria.getNome(), categoria.getTipo(), valorCategoria.get() );
            
            itensResumo.add(itemResumo);

        });
        return itensResumo;
    }

}
