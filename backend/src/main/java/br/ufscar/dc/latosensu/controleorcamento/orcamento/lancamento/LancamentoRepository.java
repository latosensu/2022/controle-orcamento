package br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface LancamentoRepository <T, ID extends Serializable> extends JpaRepository<T, ID> {
    
   /**
   * Encontra os lançamentos cuja descrição contenham a descrição passada como parâmetro
   * @param descricao Descrição que se deseja procurar
   * @return Lista de lançamento
   */
    public List<Lancamento> findAllByDescricaoILikeUltra(String descricao);

    public List<Lancamento> findByDataBalanceteBetween(LocalDate date, LocalDate date2);
                                                                                                                                                              
}
