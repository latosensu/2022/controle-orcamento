package br.ufscar.dc.latosensu.controleorcamento.orcamento.extrato;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
  path = "/extrato",
  produces = { MediaType.APPLICATION_JSON_VALUE }
)
public class ExtratoController {

  private ExtratoService extratoService;

  public ExtratoController(ExtratoService extratoService) {
    this.extratoService = extratoService;
  }

  @GetMapping("/excecao")
  public Extrato gerarExcecao() throws Exception {
    throw new Exception("Erro");
  }

  @GetMapping("")
  public Extrato gerarExtrato(
    @Valid @RequestParam(
      name = "dataInicio",
      required = false
    ) String dataInicio,
    @Valid @RequestParam(name = "dataFim", required = false) String dataFim
  ) {
    Optional<LocalDate> dataInicioExtrato;

    try {
      dataInicioExtrato = Optional.of(LocalDate.parse(dataInicio));
    } catch (Exception e) {
      dataInicioExtrato = Optional.ofNullable(null);
    }

    Optional<LocalDate> dataFimExtrato;
    try {
      dataFimExtrato = Optional.of(LocalDate.parse(dataFim));
    } catch (Exception e) {
      dataFimExtrato = Optional.ofNullable(null);
    }

    System.out.println("Deu certo");

    return this.extratoService.gerarExtrato(dataInicioExtrato, dataFimExtrato);
  }
}
