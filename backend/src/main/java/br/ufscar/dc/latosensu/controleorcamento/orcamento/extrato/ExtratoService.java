package br.ufscar.dc.latosensu.controleorcamento.orcamento.extrato;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.Lancamento;
import br.ufscar.dc.latosensu.controleorcamento.orcamento.lancamento.LancamentoRepository;

@Service
public class ExtratoService {

    private LancamentoRepository<Lancamento, Long> lancamentoRepository;

    public ExtratoService(LancamentoRepository<Lancamento, Long> lancamentoRepository){
        this.lancamentoRepository = lancamentoRepository;
    }
    public Extrato gerarExtrato(Optional<LocalDate> dataInicio, Optional<LocalDate> dataFim) {
        List<Lancamento> lancamentos = this.lancamentoRepository.findAll();

        LocalDate dataInicioExtrato = dataInicio.orElse(LocalDate.of(1970, 01, 01));
        LocalDate dataFimExtrato = dataFim.orElse(LocalDate.now());
        return new Extrato(lancamentos, dataInicioExtrato, dataFimExtrato);

    }
    
}
