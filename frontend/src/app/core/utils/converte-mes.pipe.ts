import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'converteMes'
})
export class ConverteMesPipe implements PipeTransform {

  mesesConvertidos = [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ]

  constructor() { }
  transform(mes: number): string {
    return this.mesesConvertidos[mes-1];
  }

}
