export interface ItemResumo {
  nomeCategoria: string;
  valorTotalCategoria: number;
  tipoCategoria: string;
}
