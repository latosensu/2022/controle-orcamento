export interface Categoria {
  id: number;
  nome: string;
  tipo: string;
  descricao: string;
}
